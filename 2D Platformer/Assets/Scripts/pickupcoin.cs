﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class pickupcoin : MonoBehaviour {
	public int value;
	public Text countText;
	private static int score = 0;
	// For now just destroy the coin if the player runs into
	// it. Could play a sound, add to a score or more here later!
	void OnTriggerEnter2D(Collider2D other){
		if (other.gameObject.CompareTag ("Player")) {
			score += value;
			Destroy(this.gameObject);
			//Debug.Log("Coin was picked up");
		}
		SetCountText ();
	}
	void SetCountText ()
	{
		countText.text = "Count: " + score.ToString ();
	}
}
