﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSwitch : MonoBehaviour {
	public ParticleSystem emitter;
	public ParticleSystem emitter2;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Return)) 
		{
			if (emitter.isPlaying) {
				emitter.Stop ();
				emitter.Clear ();
			} else {
				emitter.Play ();
			}
		}
		if (Input.GetKeyDown(KeyCode.RightShift)) 
		{
			if (emitter2.isPlaying) {
				emitter2.Stop ();
				emitter2.Clear ();
			} else {
				emitter2.Play ();
			}
		}
	}
}
